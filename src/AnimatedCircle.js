import Animated, {
  useSharedValue,
  withTiming,
  useAnimatedStyle,
  Easing,
  useDerivedValue,
} from "react-native-reanimated";

import { array, object, string } from "prop-types";
import Matter from "matter-js";

import React from "react";

const AnimatedStyleUpdateExample = (props) => {
  // let top = props.body.position.y;

  const [top, setTop] = React.useState(props.body.position.y);

  const onUpdate = (e) => {
    // console.log(props.body.position.y);

    setTop(props.body.position.y);

    // console.log(top)
  };

  const progress = useSharedValue(props.body.position.y);
  React.useEffect(() => {
    Matter.Events.on(props.engine, "afterUpdate", onUpdate);
  });
  const y = useDerivedValue(() => {
    return progress.value;
  }, [props.body.position.y]);

  //   const randomWidth = useSharedValue();
  const width = props.radius * 2;
  const height = props.radius * 2;
  //   const config = {
  //     duration: 500,
  //     easing: Easing.bezier(0.5, 0.01, 0, 1),
  //   };

  const style = useAnimatedStyle(() => {
    return {
      top: y.value,
    };
  });

  //   console.log(props.body);
  //   console.log(props.body.position.y);
  return (
    <Animated.View
      style={[
        {
          left: props.body.position.x,
          //   top: y,
          width,
          height,
          backgroundColor: props.color,
          borderRadius: props.radius,
        },
        style,
      ]}
    />
  );
};

export default (world, color, x, y, radius, ...rest) => {
  const body = Matter.Bodies.circle(x, y, radius, ...rest);
  Matter.World.add(world, [body]);

  return {
    body,
    radius,
    // size: [radius, radius],
    color,
    renderer: AnimatedStyleUpdateExample,
  };
};

AnimatedStyleUpdateExample.propTypes = {
  size: array,
  body: object,
  color: string,
};
