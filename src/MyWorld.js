import { Dimensions } from "react-native";
import React from "react";
import World from "./world";
import Loop from "./loop";
import rectangle from "./Rectangle";
const { height, width } = Dimensions.get("window");
// import circle from "./Circle";
import circle from "./AnimatedCircle";
import PropTypes from "prop-types";
class MyRectangle extends React.Component {
  static contextTypes = {
    engine: PropTypes.object,
  };

  render() {
    const { renderer: R, ...rest } = rectangle(
      this.context.engine.world,
      "red",
      { x: 0, y: height },
      { height: 60, width }
    );
    return <R {...rest} engine={this.context.engine} />;
  }
}
class MyCircle extends React.Component {
  static contextTypes = {
    engine: PropTypes.object,
  };

  render() {
    // const radius = Math.ceil(Math.random() * 30) + 20;
    // const x =
    //   -Math.ceil(Math.random() * -40) + Math.ceil(Math.random() * 40) + 80;
    // const y = Math.ceil(Math.random() * 40);
    // const props = {
    //   restitution: 0.7,
    //   friction: 0.5,
    // };

    const { renderer: R, ...rest } = circle(
      this.context.engine.world,
      "blue",
      80,
      100,
      10,
      { restitution: 0.7 }
      //   x,
      //   y,
      //   radius,
      //   props
    );
    // return null
    return <R {...rest} engine={this.context.engine} />;
  }
}
export default function MyWorld(props) {
  const onUpdate = (e) => {
    //   console.log(e)
    // console.log(e);
  };

  return (
    <Loop>
      <World onUpdate={onUpdate}>
        <MyRectangle />
        <MyCircle />
      </World>
    </Loop>
  );
}
